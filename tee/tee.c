#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#define NUM_THREADS 2

char name[10000];

pthread_mutex_t noinput;
pthread_cond_t print;

void waitfor() {
  pthread_mutex_lock(&noinput);
  pthread_cond_wait(&print, &noinput);
  pthread_mutex_unlock(&noinput);
}

void *output() {
  while(1) {
    waitfor();
    printf("\nSome text: %s\n", name);
  }
}

void *file() {
  while(1) {
    waitfor();
    FILE *f = fopen("test.txt", "a");
    const char *text = name;
    if (f == NULL) {
      printf("Error opening file!\n");
      exit(1);
    }
    fprintf(f, "\nSome text: %s\n", text);
    fclose(f);
  }
}


int main(int argc, char const *argv[]) {
  pthread_mutex_init(&noinput, NULL);
  pthread_cond_init(&print, NULL);
  pthread_t threads[NUM_THREADS];
  int rc, rc2;
  rc = pthread_create(&threads[0], NULL, output, (void *)name);
  rc2 = pthread_create(&threads[1], NULL, file, (void *)name);
  if (rc || rc2) {
    printf("ERROR; return code from pthread_create() is %d\n", rc);
    printf("ERROR; return code from pthread_create() is %d\n", rc2);
    exit(-1);
  }



  while(1) {
    printf("\nInput text: ");
    scanf ("%[^\n]%*c", name);
    pthread_cond_broadcast(&print);
  }
  pthread_exit(NULL);
}
