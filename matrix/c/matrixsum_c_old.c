/* matrix summation using pthreads

   features: uses a barrier; the Worker[0] computes
             the total sum from partial sums computed by Workers
             and prints the total sum to the standard output

   usage under Linux:
     gcc matrixSum.c -lpthread
     a.out size numWorkers

*/
#ifndef _REENTRANT
#define _REENTRANT
#endif
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include <sys/time.h>
#include <limits.h>
#define MAXSIZE 10000  /* maximum matrix size */
#define MAXWORKERS 10   /* maximum number of workers */

pthread_mutex_t barrier;  /* mutex lock for the barrier */
pthread_cond_t go;        /* condition variable for leaving */
int numWorkers;           /* number of workers */
int numArrived = 0;       /* number who have arrived */
int max = 0;             /* Highest number */
int low = INT_MAX;       /* Lowest number */
int lowPos[2];
int maxPos[2];

int finalSum = 0;
int matrixSum = 0;
int rowCount = 0;

int work[10];

/* a reusable counter barrier */
// void Barrier() {
//   pthread_mutex_lock(&barrier);
//   numArrived++;
//   if (numArrived == numWorkers) {
//     numArrived = 0;
//     pthread_cond_broadcast(&go);
//   } else
//     pthread_cond_wait(&go, &barrier);
//   pthread_mutex_unlock(&barrier);
// }

pthread_mutex_t maxLock;
pthread_mutex_t lowLock;
pthread_cond_t freeMax;
pthread_cond_t freeLow;

pthread_mutex_t stop;
pthread_mutex_t sumstop;

void changeMax(int *value, int i, int j)
{
  pthread_mutex_lock(&lowLock);
  if (max < *value)
  {
    max = *value;
    maxPos[0] = i;
    maxPos[1] = j;
  }

  pthread_mutex_unlock(&lowLock);
  pthread_cond_signal(&freeLow);
}

void changeLow(int *value, int i, int j)
{
  pthread_mutex_lock(&maxLock);
  if (low > *value)
  {
    low = *value;
    lowPos[0] = i;
    lowPos[1] = j;
  }

  pthread_mutex_unlock(&maxLock);
  pthread_cond_signal(&freeMax);
}

void sumWorkers(int sum, int* myRow, int* mySize, int id) {
  pthread_mutex_lock(&barrier);
    finalSum += sum;
    rowCount++;
    if (*myRow >= (*mySize - 1)) {
      if (numWorkers < 1) {
        printf("thread %d with numworkers == 1: %d\n", id, *myRow);
        pthread_cond_broadcast(&go);
      }
      else
        numWorkers--;
    }

    *myRow = rowCount;
  work[id] += 1;
  pthread_mutex_unlock(&barrier);
}

/* timer */
double read_timer() {
    static bool initialized = false;
    static struct timeval start;
    struct timeval end;
    if( !initialized )
    {
        gettimeofday( &start, NULL );
        initialized = true;
    }
    gettimeofday( &end, NULL );
    return (end.tv_sec - start.tv_sec) + 1.0e-6 * (end.tv_usec - start.tv_usec);
}

double start_time, end_time; /* start and end times */
int size, stripSize;  /* assume size is multiple of numWorkers */
int sums[MAXWORKERS]; /* partial sums */
int matrix[MAXSIZE][MAXSIZE]; /* matrix */

void *Worker(void *);

/* read command line, initialize, and create threads */
int main(int argc, char *argv[]) {
  int i, j;
  long l; /* use long in case of a 64-bit system */
  pthread_attr_t attr;
  pthread_t workerid[MAXWORKERS];

  /* set global thread attributes */
  pthread_attr_init(&attr);
  pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);

  /* initialize mutex and condition variable */
  pthread_mutex_init(&barrier, NULL);
  pthread_cond_init(&go, NULL);
  pthread_mutex_init(&maxLock, NULL);
  pthread_mutex_init(&lowLock, NULL);

  /* read command line args if any */
  size = (argc > 1)? atoi(argv[1]) : MAXSIZE;
  numWorkers = (argc > 2)? atoi(argv[2]) : MAXWORKERS;
  if (size > MAXSIZE) size = MAXSIZE;
  if (numWorkers > MAXWORKERS) numWorkers = MAXWORKERS;
  stripSize = size/numWorkers;

  /* initialize the matrix */
  for (i = 0; i < size; i++) {
	  for (j = 0; j < size; j++) {
          matrix[i][j] = rand()%99;
	  }
  }

  /* print the matrix */
#ifdef DEBUG
  for (i = 0; i < size; i++) {
	  printf("[ ");
	  for (j = 0; j < size; j++) {
	    printf(" %d", matrix[i][j]);
	  }
	  printf(" ]\n");
  }
#endif

  /* do the parallel work: create the workers */
  start_time = read_timer();

  pthread_mutex_lock(&barrier); // Lock so rowCount can't be changed by fast threads.
    for (l = 0; l < numWorkers; l++) {
      pthread_create(&workerid[l], &attr, Worker, (void *) l);
      rowCount++;
    }
    rowCount--; // lower rowcount again to not skip row 10.
  pthread_mutex_unlock(&barrier);

  // pthread_mutex_lock(&barrier);
  //   pthread_cond_wait(&go, &barrier);
  // pthread_mutex_unlock(&barrier);

  for (l = 0; l < numWorkers; l++) {
    pthread_join(workerid[l], NULL);
  }

  end_time = read_timer();
  /* print results */
  int m;
  printf("Load on each thread:\n");
  for (m = 0; m < MAXWORKERS; m++) {
    printf("Thread %d did %d rows\n", m, work[m]);
  }
  printf("Matrixsum: %d\n", matrixSum);
  printf("The total is %d\n", finalSum);
  printf("Highest value in matrix is %d at [%d, %d]\n", max, maxPos[0], maxPos[1]);
  printf("Lowest values in matrix is %d at [%d, %d]\n", low, lowPos[0], lowPos[1]);
  printf("The execution time is %g sec\n", end_time - start_time);


  pthread_exit(NULL);
}

/* Each worker sums the values in one strip of the matrix.
   After a barrier, worker(0) computes and prints the total */
void *Worker(void *arg) {
  int threadId = (int) arg;
  int myRow = (int) arg;
  while (myRow < size) {
      int total, i;

    #ifdef DEBUG
      printf("worker %d (pthread id %d) has started\n", myid, pthread_self());
    #endif

      /* determine first and last rows of my strip */
      // first = myid*stripSize;
      // last = (myid == numWorkers - 1) ? (size - 1) : (first + stripSize - 1);

      /* sum values in my strip */
      total = 0;
      for (i = 0; i < size; i++){
        total += matrix[myRow][i];
        if (matrix[myRow][i] > max)
        {
          changeMax(&matrix[myRow][i], myRow, i);
        }
        if (matrix[myRow][i] < low)
        {
          changeLow(&matrix[myRow][i], myRow, i);
        }
      }
      sumWorkers(total, &myRow, &size, threadId);
      // sums[myid] = total;
      // Barrier();
      // if (myid == 0) {
      //   total = 0;
      //   for (i = 0; i < numWorkers; i++)
      //     total += sums[i];
      //   /* get end time */
      // }
  }
}
